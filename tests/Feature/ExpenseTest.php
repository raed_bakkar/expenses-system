<?php

namespace Tests\Feature;

use App\Http\Services\ExpenseService;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use App\Models\User;
use App\Models\Expense;
use Tests\TestCase;

class ExpenseTest extends TestCase
{
    use \Illuminate\Foundation\Testing\DatabaseMigrations;

    /**
     * A basic feature test example.
     *
     * @return void
     */

    public function testExpenseCreatedSuccessfully()
    {
        $user = User::factory()->make();
        $this->actingAs($user, 'api');

        Storage::fake('expenses');
        $attach = UploadedFile::fake()->image('expense.jpg');

        $expenseData = [
            "name" => "Name Expense",
            "date" => date('Y-m-d'),
            "attach" => $attach,
            "amount" => 200,
        ];

        $checkData = $expenseData;
        unset($checkData['attach']);

        $this->json('POST', 'api/v1/expenses', $expenseData, ['Accept' => 'application/json'])
            ->assertStatus(201)
            ->assertJson([
                "expense" => $checkData,
                "message" => "Created successfully"
            ]);
    }

    public function testExpenseListedSuccessfully()
    {
        $user = User::factory()->make();
        $this->actingAs($user, 'api');

        Storage::fake('expenses');
        $attach = UploadedFile::fake()->image('expense.jpg');

        for ($i=0; $i < 2; $i++) { 

            $expenseData   = [
                "name" => "Name Expense $i",
                "date" => date('Y-m-d'),
                "attach" => $attach,
                "amount" => 200,
            ];

            (new ExpenseService)->create($expenseData);

            unset($expenseData['attach']);
            $checkData[] = $expenseData;
        }

        $this->json('GET', 'api/v1/expenses', ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                "expenses" => $checkData,
                "message" => "Retrieved successfully"
            ]);
    }

    public function testRetrieveExpenseSuccessfully()
    {
        $user = User::factory()->make();
        $this->actingAs($user, 'api');

        Storage::fake('expenses');
        $attach = UploadedFile::fake()->image('expense.jpg');

        $expenseData = [
            "name"   => "Name Expense",
            "date"   => date('Y-m-d'),
            "attach" => $attach,
            "amount" => 200,
        ];

        $expense = Expense::factory()->create($expenseData);
        
        unset($expenseData['attach']);
        $checkData = $expenseData;

        $this->json('GET', 'api/v1/expenses/' . $expense->id, [], ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                "expense" => $checkData,
                "message" => "Retrieved successfully"
            ]);
    }

    public function testExpenseUpdatedSuccessfully()
    {
        $user = User::factory()->make();
        $this->actingAs($user, 'api');

        Storage::fake('expenses');
        $attach = UploadedFile::fake()->image('expense.jpg');
        $expenseData = [
            "name" => "Name Expense",
            "date" => date('Y-m-d'),
            "attach" => $attach,
            "amount" => 200,
        ];

        $expense = (new ExpenseService)->create($expenseData);
        // ----------

        $payload = [
            "name" => "Name Expense Edit",
            "date" => date('Y-m-d'),
            "attach" => $attach,
            "amount" => 300,
            "_method" => 'put',
        ];

        $this->json('POST', 'api/v1/expenses/' . $expense->id , $payload, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                "expense" => [
                    "name" => "Name Expense Edit",
                    "date" => date('Y-m-d'),
                    "amount" => 300,
                ],
                "message" => "Updated successfully"
            ]);
    }

    public function testExpenseChangeStatusSuccessfully()
    {
        $user = User::factory()->make();
        $this->actingAs($user, 'api');

        Storage::fake('expenses');
        $attach = UploadedFile::fake()->image('expense.jpg');

        $expenseData   = [
            "name" => "Name Expense",
            "date" => date('Y-m-d'),
            "attach" => $attach,
            "amount" => 200,
        ];

        $expense = (new ExpenseService)->create($expenseData);

        // ----------

        $payload = [
            "status" => 'cancel',
        ];

        $this->json('PATCH', 'api/v1/expenses/' . $expense->id.'/status' , $payload, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                "message" => "Updated successfully"
            ]);
    }

    public function testDeleteExpense()
    {
        $user = User::factory()->make();
        $this->actingAs($user, 'api');

        Storage::fake('expenses');
        $attach = UploadedFile::fake()->image('expense.jpg');

        $expenseData   = [
            "name" => "Name Expense",
            "date" => date('Y-m-d'),
            "attach" => $attach,
            "amount" => 200,
        ];

        $expense = (new ExpenseService)->create($expenseData);

        $this->json('DELETE', 'api/v1/expenses/' . $expense->id, [], ['Accept' => 'application/json'])
            ->assertStatus(204);
    }

}
