<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\V1\AuthController;
use App\Http\Controllers\API\V1\ExpenseController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->group('/user', function (Request $request) {
//     return $request->user();
// });

Route::group(['prefix' => 'v1', 'as' => 'api.'], function () {
    
    Route::post('register-employee', [AuthController::class, 'registerEmployee']);
    Route::post('register-manager', [AuthController::class, 'registerManager']);
    Route::post('login', [AuthController::class, 'login']);
    
    Route::middleware('auth:api')->group(function () {

        Route::patch('expenses/{expense}/status', [ExpenseController::class, 'changeStatus']);
        Route::apiResource('expenses', ExpenseController::class);
    });
});