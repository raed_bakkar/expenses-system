<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Expenses') }}
        </h2>
    </x-slot>

    <div class="row">
        <div class="col-lg-12 margin-tb">
            <br />
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('expenses.create') }}" title="Create a expense"> <i class="fas fa-plus-circle"></i>
                    </a>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <table class="table table-bordered table-responsive-lg">
        <tr>
            <th>No</th>
            <th>Name</th>
            <th>Date</th>
            <th>Amount</th>
            <th>Attach</th>
            <th>Status</th>
            <th>Date Created</th>
            <th width="280px">Action</th>
        </tr>
        @if($expenses->count() == 0)
            <tr> <td colspan="8">Add first Expense for you</td> </tr>
        @endif

        @foreach ($expenses as $expense)
            <tr>
                <td>{{ ++$i }}</td>
                <td>{{ $expense->name }}</td>
                <td>{{ $expense->date }}</td>
                <td>{{ $expense->amount }}</td>
                <td><a href="{{ $expense->attach }}" target="_blank" download>Download</a></td>
                <td class="text-center uppercase">
                    <span data-id="{{ $expense->id }}" class="@if($expense->isPending()) btn btn-link btn-modal-change-status @endif status-color-{{ $expense->status }} "
                    >
                        {{ $expense->status }}
                    </span>
                </td>
                <td>{{ date_format($expense->created_at, 'Y-m-d') }}</td>
                <td>
                    <form action="{{ route('expenses.destroy', $expense->id) }}" method="POST">

                        <a href="{{ route('expenses.show', $expense->id) }}" title="show">
                            <i class="fas fa-eye text-success  fa-lg"></i>
                        </a>&nbsp;

                        <a href="{{ route('expenses.edit', $expense->id) }}">
                            <i class="fas fa-edit  fa-lg"></i>
                        </a>&nbsp;

                        @csrf
                        @method('DELETE')
                        <button type="submit" title="delete" style="border: none; background-color:transparent;">
                            <i class="fas fa-trash fa-lg text-danger"></i>
                        </button>
                    </form>
                </td>
            </tr>
        @endforeach
    </table>

    {!! $expenses->links() !!}

    @include('expenses.modal-change-status')

</x-app-layout>