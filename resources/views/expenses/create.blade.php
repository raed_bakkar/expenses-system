<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Expenses') }}
        </h2>
    </x-slot>

    <div class="row">
        <div class="col-lg-12 margin-tb">
            <br />
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('expenses.index') }}" title="Go back"> <i class="fas fa-backward "></i> </a>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6 jumbotron">
        
            @if ($errors->any())
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <form action="{{ route('expenses.store') }}" method="POST"  enctype="multipart/form-data">
                @csrf

                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Name*:</strong>
                            <input type="text" name="name" class="form-control" placeholder="Name" value="{{old('name')}}">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Date*:</strong>
                            <input type="text" name="date" class="form-control" placeholder="EX: {{date('Y-m-d')}}" value="{{old('date')}}">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Amount*:</strong>
                            <input type="number" name="amount" class="form-control" placeholder="EX: 200.00" value="{{old('amount')}}">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Attach*:</strong>(jpeg,png,jpg,gif,svg,pdf)
                            <div class="custom-file">
                                <input type="file" name="attach" class="form-control" id="chooseFile">
                                <!-- <label class="custom-file-label" for="chooseFile">Select file</label> -->
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>

            </form>
        </div>
    </div>
</x-app-layout>