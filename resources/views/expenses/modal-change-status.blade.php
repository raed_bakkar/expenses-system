<!-- The Modal -->
<div class="modal" id="modal-change-status">
    <div class="modal-dialog modal-sm">
    <div class="modal-content">
    
        <!-- Modal Header -->
        <div class="modal-header">
        <h4 class="modal-title">Change Status</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
            <select id="select-change-status" data-id="" class="select" style="width: 100%;">
                <option selected disabled>Pending</option>
                @foreach(listStatusByUserType() as $statusLabel)
                <option value="{{$statusLabel}}" class="uppercase">{{$statusLabel}}</option>
                @endforeach
            </select>
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
        <button type="button" class="btn btn-info btn-save-change-status" data-dismiss="modal">Save</button>
        </div>
        
    </div>
    </div>
</div>

@section('scripts')

<script>
    const $modal = $('#modal-change-status');
    const _token = $('meta[name="csrf-token"]').attr('content');

    $('.btn-modal-change-status').on('click', function(){
        let id = $(this).data('id');

        $modal.modal('show');

        $modal.find('#select-change-status').attr('data-id', id);
    });

    $('.btn-save-change-status').on('click', function(){

        $modal.find('.btn-save-change-status').html('Sending...');

        let status = $modal.find('#select-change-status').val();
        let id     = $modal.find('#select-change-status').attr('data-id');

        let url    = '/expenses/'+id+'/status'
        console.log({status:status, '_token': _token})
        $.ajax({
            url: url,
            data: {status:status, '_token': _token},
            type: "PATCH",
            success: function (data) {
        
                location.reload();
                
            }, error: function (XMLHttpRequest, textStatus, errorThrown) {

                if (XMLHttpRequest.status == 0) {
                    alert("Check Your Network.");
                    return;
                } else if (XMLHttpRequest.status == 404) {
                    alert("Requested URL not found.");
                    return;
                } else if (XMLHttpRequest.status == 403) {
                    alert("You does not have the right permissions.");
                    return;
                } else if (XMLHttpRequest.status == 500) {
                    alert("Internel Server Error.");
                    return;
                }    

                var errors = [];
                if("errors" in XMLHttpRequest.responseJSON) {

                Object.values(XMLHttpRequest.responseJSON.errors).map(function(e){
                    if (e[0] != "" && e[0] != " ")
                        errors.push(e[0]);
                })
                } else if( "message" in XMLHttpRequest.responseJSON && "exception" in XMLHttpRequest.responseJSON ) {

                    errors.push( XMLHttpRequest.responseJSON.message );
                }
                
                alert(errors.join('<br />'));

                $modal.find('.btn-save-change-status').html('Save');
            }
        });   
    });

</script>

@endsection