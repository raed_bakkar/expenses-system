<?php

namespace App\Models;

use App\Scopes\UserIdScope;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Expense extends Model
{
    use HasFactory;

    protected $table = 'expenses';
    public $timestamps = true;

    protected $casts = [
        'amount' => 'float',
        // 'date' => 'date'
    ];

    protected $fillable = [
        'name',
        'date',
        'amount',
        'attach',
        'status',
        'user_id'
    ];

    protected static function booted()
    {
        static::addGlobalScope(new UserIdScope);
    }

    public function setAttachAttribute($file)
    {
        if(! empty($file)) {

            $path   = $file->store('public/expenses');
            $attach = str_replace('public/', '', $path);

            $this->attributes['attach'] = $attach;
        }
    }
    
    public function getAttachAttribute()
    {
        if($this->attributes['attach']){

            return asset('storage/'.$this->attributes['attach']);
        }
    }

    public function isPending()
    {
        return $this->status == 'pending';
    }

    public function setStatus(String $status)
    {
        $this->update(['status'=>$status]);
    }
}
