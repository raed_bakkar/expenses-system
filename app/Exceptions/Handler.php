<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;
use \Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            
            if ($e instanceof ModelNotFoundException && request()->wantsJson()) {

                return response()->json(['message' => 'Not Found!'], 404);
            } else if ($e instanceof ModelNotFoundException) {

                return response()->json([ 'message' => 'Record not found', ], 404);
    
            } else if ($e instanceof \Symfony\Component\HttpKernel\Exception\NotFoundHttpException && request()->wantsJson()) {
                
                return response()->json(['message' => 'Not Found!'], 404);
            } else if ($e instanceof \Symfony\Component\HttpKernel\Exception\NotFoundHttpException) {
                // Your stuff here
                return response()->view('errors.'.$e->getStatusCode(), [], $e->getStatusCode());
            }
        });
    }

}
