<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Validator;

class ExpenseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name'   => 'required|max:255',
            'date'   => 'required|date|date_format:Y-m-d',
            'attach' => 'required|mimes:jpeg,png,jpg,gif,svg,pdf|max:5000',
            'amount' => 'required|numeric|min:0'
        ];

        if($this->method() == "PUT") {

            $rules['attach'] = 'mimes:jpeg,png,jpg,gif,svg,pdf|max:5000';
        }

        return $rules;
    }


}
