<?php

namespace App\Http\Services;

use App\Models\Expense;

class ExpenseStatusService
{
    /**
     * All List Status Can Action.
     *
     * @return Array
     */
    public function listStatus()
    {
        return [
            'pending',
            'approve',
            'reject',
            'cancel'
        ];
    }

    /**
     * List Status Can Action Manager.
     *
     * @return Array
     */
    public function listStatusCanActionManager()
    {
        return [
            'approve',
            'reject',
        ];
    }

    /**
     * List Status Can Action Employee.
     *
     * @return Array
     */
    public function listStatusCanActionEmployee()
    {
        return [
            'cancel'
        ];
    }

    /**
     * List Status By User Type.
     *
     * @return Array
     */
    public function listStatusByUserType()
    {
        if(auth()->user()->isManager()) {

            return $this->listStatusCanActionManager();
        } else {
            
            return $this->listStatusCanActionEmployee();
        }
    }

    /**
     * List Status By User Type.
     *
     * @return Array
     */
    public function getStatusForCreateByUserType()
    {
        if(auth()->user()->isManager()) {

            return 'approve';
        } else {
            
            return 'pending';
        }
    }

    /**
     * Update the Status resource in storage.
     *
     * @param  \App\Models\Expense  $expense
     * @param  String $status
     * @return \Illuminate\Http\Response
     */
    public function changeStatus(Expense $expense, String $status)
    {
        if(! $expense->isPending() ) { // Can change status only if status pending

            return response(['message' => 'Expense already have status'], 422);
        }

        $expense->setStatus($status);

        return response(['message' => 'Updated successfully'], 200);
    }
}