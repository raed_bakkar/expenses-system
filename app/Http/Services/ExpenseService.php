<?php

namespace App\Http\Services;

use App\Models\Expense;

class ExpenseService
{
    /**
     * create the Expense resource in storage.
     *
     * @param  Array  $data
     * @return \Illuminate\Http\Response
     */
    public function create($data)
    {
        $data['user_id'] = auth()->id();
        $data['status']  = (new ExpenseStatusService)->getStatusForCreateByUserType();

        return Expense::create($data);
    }

}