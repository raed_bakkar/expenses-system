<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\ExpenseRequest;
use App\Http\Requests\ExpenseStatusRequest;
use App\Models\Expense;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Resources\ExpenseResource;
use App\Http\Services\ExpenseService;
use App\Http\Services\ExpenseStatusService;

class ExpenseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Expenses = Expense::all();
        return response([ 'expenses' => ExpenseResource::collection($Expenses), 'message' => 'Retrieved successfully'], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ExpenseRequest $request)
    {
        try{
            \DB::beginTransaction();

            $data = $request->validated();

            $expense = (new ExpenseService)->create($data);
        
        }catch (\Exception $e){

            \DB::rollBack();
            \Log::error($e, ['message'=>'Expense error']);

            return response(['message' => 'Expense error'], 422);
        }
        \DB::commit();

        return response(['expense' => new ExpenseResource($expense), 'message' => 'Created successfully'], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Expense  $Expense
     * @return \Illuminate\Http\Response
     */
    public function show(Expense $Expense)
    {
        return response(['expense' => new ExpenseResource($Expense), 'message' => 'Retrieved successfully'], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Expense  $Expense
     * @return \Illuminate\Http\Response
     */
    public function update(ExpenseRequest $request, Expense $expense)
    {
        try{
            $data = $request->validated();

            $expense->update($data);
        
        }catch (\Exception $e){

            \Log::error($e, ['message'=>'Expense error']);

            return response(['message' => 'Expense error'], 422);
        }

        return response(['expense' => new ExpenseResource($expense), 'message' => 'Updated successfully'], 200);
    }

    /**
     * Update the Status resource in storage.
     *
     * @param  \App\Http\Requests\ExpenseStatusRequest  $request
     * @param  \App\Models\Expense  $Expense
     * @return \Illuminate\Http\Response
     */
    public function changeStatus(Expense $expense, ExpenseStatusRequest $request)
    {
        return (new ExpenseStatusService)->changeStatus($expense, $request->status);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Expense  $Expense
     * @return \Illuminate\Http\Response
     */
    public function destroy(Expense $expense)
    {
        $expense->delete();

        return response(['message' => 'Deleted'], 204);
    }
}