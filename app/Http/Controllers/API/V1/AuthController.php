<?php

namespace App\Http\Controllers\API\V1;


use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterRequest;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;



class AuthController extends Controller
{
    public function registerEmployee(RegisterRequest $request)
    {
        $data = $request->validated();
        $data['type'] = 'employee';

        return $this->register($data);
    }

    public function registerManager(RegisterRequest $request)
    {
        $data = $request->validated();
        $data['type'] = 'manager';

        return $this->register($data);
    }

    public function register($data)
    {
        $data['password'] = Hash::make($data['password']);

        $user = User::create($data);

        $accessToken = $user->createToken('authToken')->accessToken;

        return response(['user' => $user, 'access_token' => $accessToken], 201);
    }

    public function login(Request $request)
    {
        $loginData = $request->validate([
            'email' => 'email|required',
            'password' => 'required'
        ]);

        if (!auth()->attempt($loginData)) {
            return response(['message' => 'This User does not exist, check your details'], 400);
        }

        $accessToken = auth()->user()->createToken('authToken')->accessToken;

        return response(['user' => auth()->user(), 'access_token' => $accessToken]);
    }

    public function logout (Request $request)
    {
        $accessToken = auth()->user()->token();
        $token= $request->user()->tokens->find($accessToken);
        $token->revoke();

        return response(['message' => 'You have been successfully logged out.'], 200);
    }
}