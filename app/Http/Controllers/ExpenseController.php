<?php

namespace App\Http\Controllers;

use App\Http\Requests\ExpenseRequest;
use App\Http\Requests\ExpenseStatusRequest;
use App\Http\Services\ExpenseService;
use App\Http\Services\ExpenseStatusService;
use App\Models\Expense;
use Illuminate\Http\Request;

class ExpenseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $expenses = Expense::latest()->paginate(10);

        return view('expenses.index', compact('expenses'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('expenses.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ExpenseRequest $request)
    {
        try{
            \DB::beginTransaction();

            $data = $request->validated();

            (new ExpenseService)->create($data);
        
        }catch (\Exception $e){

            \DB::rollBack();
            \Log::error($e, ['message'=>'Expense error']);

            return redirect()->route('expenses.index')
            ->with('error', 'Expense failed to saved.');
        }
        \DB::commit();

        return redirect()->route('expenses.index')
            ->with('success', 'Expense created successfully.');        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Expense  $expense
     * @return \Illuminate\Http\Response
     */
    public function show(Expense $expense)
    {
        return view('expenses.show', compact('expense'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Expense  $expense
     * @return \Illuminate\Http\Response
     */
    public function edit(Expense $expense)
    {
        return view('expenses.edit', compact('expense'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Expense  $expense
     * @return \Illuminate\Http\Response
     */
    public function update(ExpenseRequest $request, Expense $expense)
    {
        try{
            $data = $request->validated();

            $expense->update($data);

        }catch (\Exception $e){

            \Log::error($e, ['message'=>'Expense error']);

            return redirect()->route('expenses.index')
            ->with('error', 'Expense failed to saved.');
        }

        return redirect()->route('expenses.index')
            ->with('success', 'Expense updated successfully');
    }

    /**
     * Update the Status resource in storage.
     *
     * @param  \App\Http\Requests\ExpenseStatusRequest  $request
     * @param  \App\Models\Expense  $Expense
     * @return \Illuminate\Http\Response
     */
    public function changeStatus(Expense $expense, ExpenseStatusRequest $request)
    {
        return (new ExpenseStatusService)->changeStatus($expense, $request->status);
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Expense  $expense
     * @return \Illuminate\Http\Response
     */
    public function destroy(Expense $expense)
    {
        $expense->delete();

        return redirect()->route('expenses.index')
            ->with('success', 'Expense deleted successfully');
    }
}
