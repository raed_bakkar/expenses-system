# README #

This README would normally document Expenses mini system steps to get your application up and running.

### How do I get set up? ###

* Clone the repository
git clone https://raed_bakkar@bitbucket.org/raed_bakkar/expenses-system.git

* Switch to the repo folder
cd expenses-system

* Install all the dependencies using composer
composer install

* Laravel Breeze UI Authentication 
npm install
npm run dev

* Copy the example env file and make the required configuration changes in the .env file
cp .env.example .env

* Database configuration
Create Database Mysql and edit .env file DatabaseName Username Password

* Generate a new application key
php artisan key:generate

* Run the database migrations (Set the database connection in .env before migrating)
php artisan migrate

* Generate a new passport authentication secret key
php artisan passport:install

* Create the symbolic link, you may use the storage:link
php artisan storage:link

* How to run tests Successfully
vendor/bin/phpunit

* Create Manager
php artisan manager:make

* Create Employee
php artisan employee:make

* Start the local development server
php artisan serve

* API
php artisan passport:install

php artisan serve

Open Postman APP
import Expenses System.postman_collection.json
import Expenses System.postman_environment.json

